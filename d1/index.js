

function greetHello(name){
	return `Hello ${name}`; //we use this, to return a value, but not print it on our console dev tools, we use this on our front ends
	// console.log(`Hello ${name}`);
}
greetHello("Juan");


/*
	if-else statement
	decision making of our program flow
*/

let sagotNgNililigawanKo = true; 
/*
kapag true - sinasagot niya ako, kami na
kapag false - busted ako, hindi niya ako gusto
*/

if(sagotNgNililigawanKo){
	console.log("Yehey! Kami na! Hindi na ako kasama sa SMP");
} else { //kapag false
	console.log("Inuman nalang ng redhorse, kasama nanaman sa SMP");
}


/*Loops*/

/*Instruction: Display "Juan Dela Cruz" on our console 10x*/

console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");


/*Instruction: Display each element available on our array*/

let students = ["TJ", "Mia", "Tin", "Chris"];

console.log(students[0]);
console.log(students[1]);
console.log(students[2]);
console.log(students[3]);

/*While Loop*/
let count = 5; //number of the iteration, number of times of how many we repeat our code

// while(/*condition*/){ //condition - evaluates a given code if it is true or false - if the condition is true, the loop will start and continue our iteration or the repetition of our block of code, but if the condition is false the loop or the repetition will stop
// 	//block of code - this will be repeated by the loop
// 	//counter for our iteration - this is the reason of continuous loop/iteration
// }
/*example
	Instructions: Repeat a name "Sylvan" 5x
*/
		//0 !== 0? -> false
while(count !== 0){ //condition - if count value if not equal to zero 
	console.log("Sylvan");
	count--; //will be decremented by 1
	//1-1 = 0
}
/*
	Sylvan
	Sylvan
	Sylvan
	Sylvan
	Sylvan
*/

/*Instruction: Print numbers 1 to 5 using while*/

let number = 1;
		//6 <= 5? true
while(number <= 5){ //if the number is less than or equal to 5
	console.log(number);
	number++; //5+1 = 6
}

/*
	1
	2
	3
	4
	5
*/

/*Instruction: With a given array, kindly print each element using while loop*/

let fruits = ['Banana', 'Mango'];
// fruits[0]
// fruits[1]

let indexNumber = 0; //We will use this variable as our reference to the index position/number of our given array
	//2 <= 1? false
while(indexNumber <= 1){ // the condition is based on the last index of elements that we have on an array
	console.log(fruits[indexNumber]); //-> fruits[1]
	 //kukuhanin natin yung elements sa loob ng array base sa indexNumber value
	indexNumber++; //1 + 1 = 2
}

/*
	Expected output:
	Banana
	Mango
*/

let mobilePhones = ['Samsung Galaxy S21', 'Iphone 13 Pro', 'Xioami 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus Rog 6', 'Nokia', 'Cherry Mobile'];

console.log(mobilePhones.length);
console.log(mobilePhones.length - 1); //will give us the last index position of an element in an array
console.log(mobilePhones[mobilePhones.length - 1]); //get the last element of an array

let indexNumberForMobile = 0;

while(indexNumberForMobile <= mobilePhones.length - 1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

/*Do-While - do the statement once, before going to the condition*/

let countA = 1;

do { //execute the statement
	console.log("Juan");
	countA++; //6+1 = 7
} while(countA <= 6); //7 <= 6? false

/*
	Juan
	Juan
	Juan
	Juan
	Juan
	Juan
*/
console.log("=================Do-While VS While====================");

let countB = 6;

do {
	console.log(`Do-While count ${countB}`);
	countB--;
} while(countB == 7);


/*Versus*/

while(countB == 7) {
	console.log(`While count ${countB}`);
	countB--;
}

/*
	Mini activity
	Instruction: With a given array, kindly display each elements on the console using do-while loop
*/

let indexNumberA = 0;

let computerBrands = ['Apple Macbook Pro', 'HP NoteBook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];
//8:58pm

do {
	console.log(computerBrands[indexNumberA])
	indexNumberA++;
}while (indexNumberA <= computerBrands.length -1);


/*For Loop*/
	//variable - the scope of the declared variable is within the for loop
	//condition
	//iteration
					//-1 >= 0 ? true //0-1 = -1
for(let count = 5; count >= 0; count--){
	console.log(count);
}
/*
	5
	4
	3
	2
	1
	0
*/

/*Instruction: Given an Array, kindly print each element using for loop*/

let colors = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'White','Black'];
//9:29

for(let i = 0; i <= colors.length - 1; i++){
	console.log(colors[i]);
}

//Continue & Break
//Break - stops the execution of our code
//Continue - skip a block code and continue to the next iteration

/* 
ages
	18, 19, 20, 21, 24, 25
	age == 21 (debutante age of boys), we will skip then go to the next iteration
	18, 19, 20, 24, 25

*/

let ages = [18, 19, 20, 21, 24, 25]; 
/*Skip the debutante of boys and girls using continue keyword*/
				//2 <= 5 ? true //1+1 = 2
for(let i = 0; i <= ages.length - 1; i++){
		//ages[3] -> 21
	if(ages[i] == 21 || ages[i] == 18){
		continue;
	}

	console.log(ages[i]);
}


/*
	let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];

	once we found Jayson on our array, we will stop the loop

	Den
	Jayson
*/
let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
				//1 <= 3? true	//0+1 = 1
for (let i = 0;  i <= studentNames.length -1; i++) {
	//studentNames[1] => Jayson
	if(studentNames[i] == "Jayson"){
		console.log(studentNames[i]);
		break;
	}
}