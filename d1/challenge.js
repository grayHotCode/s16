//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];


for(let i = 0; i <= adultAge.length - 1; i++){
    //ages[3] -> 21
  if(adultAge[i] == 18 || adultAge[i] == 19 ||  adultAge[i] == 12 || adultAge[i] == 15 ){
    continue;
  }

  console.log(adultAge[i]);
}


/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/


// let ages = [18, 19, 20, 21, 24, 25]; 
// /*Skip the debutante of boys and girls using continue keyword*/
//         //2 <= 5 ? true //1+1 = 2
// for(let i = 0; i <= ages.length - 1; i++){
//     //ages[3] -> 21
//   if(ages[i] == 21 || ages[i] == 18){
//     continue;
//   }

//   console.log(ages[i]);
// }


/*
Instructions:



2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	//add solutions here
  //you can refer on our last discussion yesterday

  for (let i = 0; i <= students.length -1; i++) {
    if(students[i] == "Jazz"){
      console.log(students[i]);
      break;
    }
  }
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
	Sample output:
  
  Jazz
*/


// let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
//         //1 <= 3? true  //0+1 = 1
// for (let i = 0;  i <= studentNames.length -1; i++) {
//   //studentNames[1] => Jayson
//   if(studentNames[i] == "Jayson"){
//     console.log(studentNames[i]);
//     break;
//   }
// }